`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:43:25 06/06/2018 
// Design Name: 
// Module Name:    command_module 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: This module identifies received commands from the UART. when it detects an
// 	exclamation point '�', Dec: 231, Hex 0xE7, then it goes into alert mode where the next
//		eight bit character modifies the behavior of the ADC. The commands are as follows:
//
//		1-190: sets the clock rate of the multispeed clock module at a value between
//				 1k to 190k samples per second
//
//		208: Sample endlessly
//		209: 1000 Samples
//		210: 1 million samples
//		211: 10 million samples
//	
//		191: Start sampling (turns adc on)
//		192: Stop sampling (turns adc off)
//
//		240-255: Additional commands need to be written in so that the number of channels
//				to be sampled can be set. Will probably be something such that the least
//				significant nibble can vary from 1-15 freely and the number can be easily
//				forwarded to the CBF.
//				For example F0 = 240 and FF =255 so between these two values inclusive,
//				can be used to describe all possible variations of four input channels
//
//		Free command addresses:
//			193-207
//			212-239
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module command_module(

	 //input from the UART that tells the clock what frequency to generate a clock
	 // and for how long to generate the clock for
    input [7:0] charIn,
	 
	 //loads in charIn
    input load,
	 
	 //400KHz clk
    input clk_in,
	input reset,
	 
	 //represents in kilohertz the frequency of the ADC clock
	 output reg [7:0] clkspd,
	 
	 //represents the sequence length
	 //0 = forever
	 //1 = 1000
	 //2 = 1M
	 //3 = 10M
	 output reg [1:0] seq_len,
	 
	 //represents whether the clock output should be enabled
	 //or disabled.
	 //	1 = enabled
	 //	0 = disabled
	 output reg sample_state,
	 
	 //value that goes to the CBF to tell which if any ADC channels
	 //to enable
	 output reg [3:0] channel_num,
	 
	 //goes to the Multispeed clock module to load in a new command
	 output reg load_out
    );

	//register indicates whether a command signal has been detected
	reg cmd_state;
	/*
	initial begin
		cmd_state 		<= 0;
		load_out	 	<= 0;
		channel_num 	<= 0;
		sample_state 	<= 0;
	end
	*/
	
	
	always@(posedge clk_in)
	begin
		
		//reset
		if(reset)
		begin
			//after reset it is set to 7ksps, 1000 samples
			//and only the first adc
			clkspd 			<= 7;
			seq_len  		<= 1;
			sample_state	<= 1;
			cmd_state		<= 0;
			load_out		<= 0;
			channel_num 	<= 1;
		end
		
		//else the rest of the module
		else
		begin
			//load ==1
			if(load == 1)
			begin
				//if cmd_state == 0
				// check if input char is '�'
				if(cmd_state == 0)
				begin
						//if input char == �
						if(charIn == "�")
						begin
							cmd_state 		<= 1;
							load_out		<= 0;
						end
						
						//if input char doesn't equal � then do nothing
						else
						begin
							load_out		<= 0;
							cmd_state 		<= 0;
						end
				end
				
				//if cmd_state == 1
				//check which command it is
				else begin
					//set cmd_state to 0
					cmd_state <=0;
				
		
					//if charIn == 1-190
					if(charIn <=190 && charIn >=1)
					begin
						clkspd 			<= charIn;
						sample_state 	<= 0;
						load_out		<= 1;
					end
					
					//if charIn == 208-211
					//set sequence length
					else if(charIn <=211 && charIn >=208)
					begin
						seq_len 		<= charIn[1:0];
						sample_state 	<= 0;
						load_out		<= 1; 
					end
					
					//if charIn == 191
					//start sampling
					else if(charIn == 191)
					begin
						sample_state 	<= 1;
						load_out		<= 1;
					end
					
					//if charIn == 5
					//stop sample
					else if(charIn == 192)
					begin
						sample_state 	<= 0;
						load_out		<= 1;
					end
			
					
					//if charIn >= 240 and <=255
					//change the number of adc channels
					else if(charIn <= 255 && charIn >= 240)
					begin
						channel_num 	<= charIn[3:0];
						sample_state 	<= 0;
						load_out		<= 1;
					end
					
					//else do nothing
					else
					begin
						load_out		<= 0;
					end
					
					
				end
				
			end
			
			else 
			begin
				load_out		<= 0;
			end
			
			
			
		end
		
	end


endmodule
