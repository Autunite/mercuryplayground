`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/24/2020 11:54:35 PM
// Design Name: 
// Module Name: pwmLed_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
//`include "pwmLed.v"

module pwmLed_tb;

	//inputs
	reg clock;
	reg reset;
	
	
	//outputs
	wire LED_OUT;
	wire NOT_LED_OUT;
	
	pwmLED uut (
	.clk(clock),
	.reset_n(reset),
	.PL_LED_G(LED_OUT),
	.NOT_PL_LED_G(NOT_LED_OUT)
	
	);
	
	initial begin
	
	
	
	// Wait 100 ns for global reset to finish
		#100;
		
		clock = 0;
		reset = 1;
		
		#20;
		
		reset = 0;
		
		#20;
		
		reset = 1;
	
	end
	
	//clk gen
	always #10  clock =  ! clock;

endmodule
