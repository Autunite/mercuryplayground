module pwmLED(
		input clk, 
		input reset_n,
		output PL_LED_G,
		output NOT_PL_LED_G
		);
reg [25:0] count;
reg [7:0] pwm;
always @(posedge clk or negedge reset_n)
begin
	if(!reset_n) 
	begin
		count<=0;
		pwm<=0;
	end 
	
	else
	begin
		count<=count+1;
		pwm <= pwm[6:0]+pwm_input;
   	end
end

wire [6:0] pwm_input = count[25] ? count[24:18] : ~count[24:18];

/* always @(posedge clk)
begin
   pwm <= pwm[6:0]+pwm_input;
end */

assign PL_LED_G = pwm[7];
assign NOT_PL_LED_G = ~pwm[7];
endmodule 