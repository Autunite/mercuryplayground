`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:18:53 08/25/2017 
// Design Name: 
// Module Name:    clockdiv25to6point25MHz 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module clockdiv25to6point25MHz(
    input twentyFiveMHzClk,
    output sixPointTwoFiveClk
    );

reg [1:0] count = 3'b000;

always @(posedge twentyFiveMHzClk)
begin
	count <= count + 1;
end

assign sixPointTwoFiveClk = count[1];

endmodule
