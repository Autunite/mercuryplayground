`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date:    09:22:35 05/23/2018 
// Design Name: 
// Module Name:    nibbleAddr 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: Sign extends and conditionally adds 48 or 55 to the input number
// to make the nibble into an ASCII char
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module nibbleAddr(
    input [3:0] binaryIn,
	input clk,
    output reg [7:0] nibbleASCIIOut
    );
	 
	 reg [7:0] character;
	 
	 
	 /*
	 //sign extend
	 assign character = {4'b0000 , binaryIn};
	// if input is 9 or lower add 48 for number char, if greater then add 55 for character char
	assign nibbleASCIIOut = (binaryIn <= 9) ? character + 48 : character + 55;
	*/
	
	always@(posedge clk)
		begin
			character <= {4'b0000 , binaryIn};
			
			if(binaryIn <= 9)
				begin
					nibbleASCIIOut <= character + 48; 
				end
			else
				begin
					nibbleASCIIOut <= character + 55;
				end
			
		end

endmodule
