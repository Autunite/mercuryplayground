`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date:    15:37:19 05/23/2018 
// Design Name: 
// Module Name:    FIFO32x8 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: This device is a FIFO memory that takes in up to 4 32 bit inputs and breaks them up
// into 8 bit characters and places them in memory based off the current contents of the memory, and
// shifts out the first places piece of through the out port. It does this using a mux, a 32x8 shift
// register, a 5 bit pointer, and a 2 bit imput indicating the number of 32bit pieces of data currently
// incoming. At the end of the inputed data an 8 bit number representing an ASCII comma is placed in,
// and the pointer is properly incremented to take into account the characters placed in and the ending 
// comma.
//
// When the shift out line is asserted the pointer is decremented by one and all the data is shifted up
// by one address, with the lowest address (0) being erased. 
//
// Reset clears all memory
//
// If the memory is full to the point that the next four data pieces threaten to overfill it then isFull
// is asserted and the memory refuses to take in more data.
//
// When latch in is set to one, the data is latched in one full clock cycle after the first clk rising
// edge detects the change in latch in
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module FIFO32x8(
    input [31:0] in0,
    input [31:0] in1,
    input [31:0] in2,
    input [31:0] in3,
    input [1:0] inNum,
    input latchIn,
    input latchOut,
    input reset,
    input clk,
    output reg [7:0] out,
    output 		 isFull ,
	output  	isEmpty
    );
	 
	 
	
	//wires reg's instantiation
	
	//wire inShot, outShot;
	reg [5:0] readAddr = 0;
	reg [5:0] writeAddr = 0;
	reg [7:0] iterator = 0;
	
	//memory block instantiation
	reg [7:0] ram_array [63:0];
	
	
	
	//Figure out the full and empty flags
	//Circular buffer is used so figure out how to deal
	//with detecting whether the write pointer gets within
	//a critical range of the read pointer
	//Care must be taken to make sure that wrap around
	//logic of the circular buffer
	
	//assignment to update the isFull flag
	//doesn't work with edge cases
	//assign isFull = (writeAddr+17 >=readAddr) ? 1:0;
	
	//assignment to update the isEmpty flag
	assign isEmpty = (writeAddr==readAddr) ? 1:0;
	
	
	
	//main part of the module
	//set to zero upon reset
	//when writing to the ram make sure that
	//latchIn and latchOut are 1 for only one clock cycle
	//putting in a one shot built in so that they are internal
	//to this module
	//
	always@(posedge clk)
	begin
	//reset memory to 0
		if(reset)
		begin
			readAddr 	<= 8'h00;
			writeAddr 	<= 8'h00;
			out 		<= 8'h00;
			for(iterator = 0 ; iterator <64; iterator = iterator +1)
			begin
				ram_array[iterator] <= 0;
			end
		end
		
		/*
			case statement to control reading and writing actions to the
			FIFO. Should handle all cases, as long as inNum is always correct
			i.e. if a write action is done and in num is 00 then it will always
			assume that one chunk of data was sent, even if nothing was written
			to the input.
		*/
		//put in full, empty, write, read, inNum for the case sensitivity list
		else casez({isFull, isEmpty, latchIn, latchOut, inNum})
		
			//reg [7:0] readAddr = 0;
			//reg [7:0] writeAddr = 0;
		
			// buffer isn't full, isn't empty, read asserted, write asserted
			6'b0011?? : begin
				//inNum 0
				if(inNum == 2'b00)
				begin
					writeAddr 					<= writeAddr +5;
					ram_array[writeAddr] 	<= in0[31:24];
					ram_array[writeAddr+1] 	<= in0[23:16];
					ram_array[writeAddr+2] 	<= in0[15:8];
					ram_array[writeAddr+3] 	<= in0[7:0];
					ram_array[writeAddr+4] 	<= ",";
					//add in read statement
					//update read address
					readAddr 					<= readAddr+1;
					out							<= ram_array[readAddr];
				end
				
				//inNum 1
				else if(inNum == 2'b01)
				begin
					writeAddr 					<= writeAddr +9;
					ram_array[writeAddr] 	<= in0[31:24];
					ram_array[writeAddr+1] 	<= in0[23:16];
					ram_array[writeAddr+2] 	<= in0[15:8];
					ram_array[writeAddr+3] 	<= in0[7:0];
					ram_array[writeAddr+4] 	<= in1[31:24];
					ram_array[writeAddr+5] 	<= in1[23:16];
					ram_array[writeAddr+6] 	<= in1[15:8];
					ram_array[writeAddr+7] 	<= in1[7:0];
					ram_array[writeAddr+8] 	<= ",";
					//add in read statement
					//update read address
					readAddr 					<= readAddr+1;
					out							<= ram_array[readAddr];
					
				end
				
				//inNum 2
				else if(inNum == 2'b10)
				begin
					writeAddr 						<= writeAddr +13;
					ram_array[writeAddr] 		<= in0[31:24];
					ram_array[writeAddr+1] 		<= in0[23:16];
					ram_array[writeAddr+2] 		<= in0[15:8];
					ram_array[writeAddr+3] 		<= in0[7:0];
					ram_array[writeAddr+4] 		<= in1[31:24];
					ram_array[writeAddr+5] 		<= in1[23:16];
					ram_array[writeAddr+6] 		<= in1[15:8];
					ram_array[writeAddr+7] 		<= in1[7:0];
					ram_array[writeAddr+8] 		<= in2[31:24];
					ram_array[writeAddr+9] 		<= in2[23:16];
					ram_array[writeAddr+10] 	<= in2[15:8];
					ram_array[writeAddr+11] 	<= in2[7:0];
					ram_array[writeAddr+12] 	<= ",";
					//add in read statement
					//update read address
					readAddr 					<= readAddr+1;
					out							<= ram_array[readAddr];
					
				end
				
				//inNum 3
				else if(inNum == 2'b11)
				begin
					writeAddr 					<= writeAddr +17;
					ram_array[writeAddr] 		<= in0[31:24];
					ram_array[writeAddr+1] 		<= in0[23:16];
					ram_array[writeAddr+2] 		<= in0[15:8];
					ram_array[writeAddr+3] 		<= in0[7:0];
					ram_array[writeAddr+4] 		<= in1[31:24];
					ram_array[writeAddr+5] 		<= in1[23:16];
					ram_array[writeAddr+6] 		<= in1[15:8];
					ram_array[writeAddr+7] 		<= in1[7:0];
					ram_array[writeAddr+8] 		<= in2[31:24];
					ram_array[writeAddr+9] 		<= in2[23:16];
					ram_array[writeAddr+10] 	<= in2[15:8];
					ram_array[writeAddr+11] 	<= in2[7:0];
					ram_array[writeAddr+12] 	<= in3[31:24];
					ram_array[writeAddr+13] 	<= in3[23:16];
					ram_array[writeAddr+14] 	<= in3[15:8];
					ram_array[writeAddr+15] 	<= in3[7:0];
					ram_array[writeAddr+16] 	<= ",";
					//add in read statement
					//update read address
					readAddr 					<= readAddr+1;
					out							<= ram_array[readAddr];
					
				end
				
			end
			
			
			// buffer is full and an attempt to write is made
			6'b1?1??? : begin
			
			end
			
			// buffer is empty and an attempt to read is made
			6'b?1?1?? : begin
			
			end
			
			// buffer isn't full, write asserted, read not asserted
			6'b0?10?? : begin
				//inNum 0
				if(inNum == 2'b00)
				begin
					writeAddr 					<= writeAddr +5;
					ram_array[writeAddr] 	<= in0[31:24];
					ram_array[writeAddr+1] 	<= in0[23:16];
					ram_array[writeAddr+2] 	<= in0[15:8];
					ram_array[writeAddr+3] 	<= in0[7:0];
					ram_array[writeAddr+4] 	<= ",";
				end
				
				//inNum 1
				else if(inNum == 2'b01)
				begin
					writeAddr 					<= writeAddr +9;
					ram_array[writeAddr] 	<= in0[31:24];
					ram_array[writeAddr+1] 	<= in0[23:16];
					ram_array[writeAddr+2] 	<= in0[15:8];
					ram_array[writeAddr+3] 	<= in0[7:0];
					ram_array[writeAddr+4] 	<= in1[31:24];
					ram_array[writeAddr+5] 	<= in1[23:16];
					ram_array[writeAddr+6] 	<= in1[15:8];
					ram_array[writeAddr+7] 	<= in1[7:0];
					ram_array[writeAddr+8] 	<= ",";
				end
				
				//inNum 2
				else if(inNum == 2'b10)
				begin
					writeAddr 						<= writeAddr +13;
					ram_array[writeAddr] 		<= in0[31:24];
					ram_array[writeAddr+1] 		<= in0[23:16];
					ram_array[writeAddr+2] 		<= in0[15:8];
					ram_array[writeAddr+3] 		<= in0[7:0];
					ram_array[writeAddr+4] 		<= in1[31:24];
					ram_array[writeAddr+5] 		<= in1[23:16];
					ram_array[writeAddr+6] 		<= in1[15:8];
					ram_array[writeAddr+7] 		<= in1[7:0];
					ram_array[writeAddr+8] 		<= in2[31:24];
					ram_array[writeAddr+9] 		<= in2[23:16];
					ram_array[writeAddr+10] 	<= in2[15:8];
					ram_array[writeAddr+11] 	<= in2[7:0];
					ram_array[writeAddr+12] 	<= ",";
				end
				
				//inNum 3
				else if(inNum == 2'b11)
				begin
					writeAddr 						<= writeAddr +17;
					ram_array[writeAddr] 		<= in0[31:24];
					ram_array[writeAddr+1] 		<= in0[23:16];
					ram_array[writeAddr+2] 		<= in0[15:8];
					ram_array[writeAddr+3] 		<= in0[7:0];
					ram_array[writeAddr+4] 		<= in1[31:24];
					ram_array[writeAddr+5] 		<= in1[23:16];
					ram_array[writeAddr+6] 		<= in1[15:8];
					ram_array[writeAddr+7] 		<= in1[7:0];
					ram_array[writeAddr+8] 		<= in2[31:24];
					ram_array[writeAddr+9] 		<= in2[23:16];
					ram_array[writeAddr+10] 	<= in2[15:8];
					ram_array[writeAddr+11] 	<= in2[7:0];
					ram_array[writeAddr+12] 	<= in3[31:24];
					ram_array[writeAddr+13] 	<= in3[23:16];
					ram_array[writeAddr+14] 	<= in3[15:8];
					ram_array[writeAddr+15] 	<= in3[7:0];
					ram_array[writeAddr+16] 	<= ",";
				end
				
			end	
				
				
			
			// buffer isn't empty, write is not asserted, read asserted
			6'b?001?? : begin
				out <= ram_array[readAddr];
				readAddr <= readAddr+1;
			end
			
			

		//default case if none of the above are true
		//do nothing
		default: begin end
		endcase
		
	end
	
	
	 
	 fullChecker comparator(
    .rIndex(readAddr),
    .wIndex(writeAddr),
    .isFull(isFull)
    );

endmodule
