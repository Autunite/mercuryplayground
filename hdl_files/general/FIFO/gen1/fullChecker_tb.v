`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:38:56 06/04/2018
// Design Name:   fullChecker
// Module Name:   C:/Users/George/Documents/work/EPCPOWER/Uartchallenge/fullChecker_tb.v
// Project Name:  Uartchallenge
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: fullChecker
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module fullChecker_tb;

	// Inputs
	reg [7:0] rIndex;
	reg [7:0] wIndex;

	// Outputs
	wire isFull;

	// Instantiate the Unit Under Test (UUT)
	fullChecker uut (
		.rIndex(rIndex), 
		.wIndex(wIndex), 
		.isFull(isFull)
	);

	initial begin
		// Initialize Inputs
		rIndex = 0;
		wIndex = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		rIndex = 0;
		wIndex = 47;
		
		#5;
		rIndex = 0;
		wIndex = 46;
		
		#5;
		rIndex = 1;
		wIndex = 48;
		
		#5;
		rIndex = 1;
		wIndex = 47;
		
		#5;
		rIndex = 62;
		wIndex = 45;
		
		#5;
		rIndex = 62;
		wIndex = 44;
		
		#5;
		rIndex = 63;
		wIndex = 46;
		
		#5;
		rIndex = 63;
		wIndex = 45;
		
	end
      
endmodule

