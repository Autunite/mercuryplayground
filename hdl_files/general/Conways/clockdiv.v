`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    00:22:51 08/24/2017 
// Design Name: 
// Module Name:    clockdiv 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module clockdiv(
    input clk,
	 input reset,
    output reg divClk
    );
reg [27:0] count = 8'd0;

always@(posedge clk)
begin
	if(reset ==1)
	begin
		divClk <= 0;
		count<=0;
	end
	else
	begin
		if(count==1000)
		begin
			count<=0;
			divClk<= !divClk;
		end
		else
		begin
			count<=count+1; 
		end
	end
end

endmodule
