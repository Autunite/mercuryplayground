`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:58:10 09/14/2017 
// Design Name: 
// Module Name:    eightByEightCell 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//		This combines four 4x4 cells into a single 8x8 cell
//		like the rest of the cell types this is kept modular for future expansion
//		The current plan is to map the outer neighbors to each other to create an 8x8
//		toroidal manifold, whether to create another file to wrap this, or add in extra
//		wire busses is yet to be decided. But the former is easier to debug or maintain
//		while the latter is the lazier way to do it
//////////////////////////////////////////////////////////////////////////////////
module eightByEightCell(
    input [91:0] neighbors,
    input clk,
	 input load,
    input [7:0] in0,
    input [7:0] in1,
    input [7:0] in2,
    input [7:0] in3,
    input [7:0] in4,
    input [7:0] in5,
    input [7:0] in6,
    input [7:0] in7,
    output [7:0] out0,
    output [7:0] out1,
    output [7:0] out2,
    output [7:0] out3,
    output [7:0] out4,
    output [7:0] out5,
    output [7:0] out6,
    output [7:0] out7
    );

wire [43:0] fxf00neighbor;
wire [43:0] fxf10neighbor;
wire [43:0] fxf01neighbor;
wire [43:0] fxf11neighbor;

//upper left four by four
fourByFourCell fxf00(
    .neighbors(fxf00neighbor),
    .clk(clk),
    .load(load),
    .in0(in0[7:4]),
    .in1(in1[7:4]),
    .in2(in2[7:4]),
    .in3(in3[7:4]),
    .out0(out0[7:4]),
    .out1(out1[7:4]),
    .out2(out2[7:4]),
    .out3(out3[7:4])
    );
	 
//upper right four by for
fourByFourCell fxf10(
    .neighbors(fxf10neighbor),
    .clk(clk),
    .load(load),
    .in0(in0[3:0]),
    .in1(in1[3:0]),
    .in2(in2[3:0]),
    .in3(in3[3:0]),
    .out0(out0[3:0]),
    .out1(out1[3:0]),
    .out2(out2[3:0]),
    .out3(out3[3:0])
    );

//bottom left four by four
fourByFourCell fxf01(
    .neighbors(fxf01neighbor),
    .clk(clk),
    .load(load),
    .in0(in4[7:4]),
    .in1(in5[7:4]),
    .in2(in6[7:4]),
    .in3(in7[7:4]),
    .out0(out4[7:4]),
    .out1(out5[7:4]),
    .out2(out6[7:4]),
    .out3(out7[7:4])
    );

//bottom right four by four
fourByFourCell fxf11(
    .neighbors(fxf11neighbor),
    .clk(clk),
    .load(load),
    .in0(in4[3:0]),
    .in1(in5[3:0]),
    .in2(in6[3:0]),
    .in3(in7[3:0]),
    .out0(out4[3:0]),
    .out1(out5[3:0]),
    .out2(out6[3:0]),
    .out3(out7[3:0])
    );

//assigns the four by fours to the exterior ports and to the outputs of other four by four cells	
//43 down to zero
//zero is in the upper left of the fourbyfour, 
//the angled corner connection coming in from the upper left
//43 is the connection directly clockwise to zero

//neighbors for the upper left fourbyfour
assign fxf00neighbor= 
{
//43
neighbors[91],
//42
neighbors[90],
//41
neighbors[89],
//40
neighbors[88],
//39
neighbors[87],
//38
neighbors[86],
//37
neighbors[85],
//36
neighbors[84],
//35
neighbors[83],
//34
neighbors[82],
//33
neighbors[81],
//32
out0[3],
//31
out1[3],
//30
out0[3],
//29
out1[3],
//28
out2[3],
//27
out1[3],
//26
out2[3],
//25
out3[3],
//24
out2[3],
//23
out3[3],
//22
out4[3],
//21
out4[4],
//20
out4[5],
//19
out4[4],
//18
out4[5],
//17
out4[6],
//16
out4[5],
//15
out4[6],
//14
out4[7],
//13
out4[6],
//12
out4[7],
//11
neighbors[11],
//10
neighbors[10],
//9
neighbors[9],
//8
neighbors[8],
//7
neighbors[7],
//6
neighbors[6],
//5
neighbors[5],
//4
neighbors[4],
//3
neighbors[3],
//2
neighbors[2],
//1
neighbors[1],
//0
neighbors[0]
};

//neighbors for the upper right fourbyfour
assign fxf10neighbor=  
{
//43
neighbors[79],
//42
neighbors[78],
//41
neighbors[77],
//40
neighbors[76],
//39
neighbors[75],
//38
neighbors[74],
//37
neighbors[73],
//36
neighbors[72],
//35
neighbors[71],
//34
neighbors[70],
//33
neighbors[69],
//32
neighbors[68],
//31
neighbors[67],
//30
neighbors[66],
//29
neighbors[65],
//28
neighbors[64],
//27
neighbors[63],
//26
neighbors[62],
//25
neighbors[61],
//24
neighbors[60],
//23
neighbors[59],
//22
neighbors[58],
//21
out4[0],
//20
out4[1],
//19
out4[0],
//18
out4[1],
//17
out4[2],
//16
out4[1],
//15
out4[2],
//14
out4[3],
//13
out4[2],
//12
out4[3],
//11
out4[4],
//10
out3[4],
//9
out2[4],
//8
out3[4],
//7
out2[4],
//6
out1[4],
//5
out2[4],
//4
out1[4],
//3
out0[4],
//2
out1[4],
//1
out0[4],
//0
neighbors[80]
};

//neighbors for the bottom left fourbyfour
assign fxf01neighbor=  
{
//43
out3[7],
//42
out3[6],
//41
out3[7],
//40
out3[6],
//39
out3[5],
//38
out3[6],
//37
out3[5],
//36
out3[4],
//35
out3[5],
//34
out3[4],
//33
out3[3],
//32
out4[3],
//31
out5[3],
//30
out4[3],
//29
out5[3],
//28
out6[3],
//27
out5[3],
//26
out6[3],
//25
out7[3],
//24
out6[3],
//23
out7[3],
//22
neighbors[34],
//21
neighbors[33],
//20
neighbors[32],
//19
neighbors[31],
//18
neighbors[30],
//17
neighbors[29],
//16
neighbors[28],
//15
neighbors[27],
//14
neighbors[26],
//13
neighbors[25],
//12
neighbors[24],
//11
neighbors[23],
//10
neighbors[22],
//9
neighbors[21],
//8
neighbors[20],
//7
neighbors[19],
//6
neighbors[18],
//5
neighbors[17],
//4
neighbors[16],
//3
neighbors[15],
//2
neighbors[14],
//1
neighbors[13],
//0
neighbors[12]
};

//neighbors for the bottom right fourbyfour
assign fxf11neighbor=  
{
//43
out3[3],
//42
out3[2],
//41
out3[3],
//40
out3[2],
//39
out3[1],
//38
out3[2],
//37
out3[1],
//36
out3[0],
//35
out3[1],
//34
out3[0],
//33
neighbors[57],
//32
neighbors[56],
//31
neighbors[55],
//30
neighbors[54],
//29
neighbors[53],
//28
neighbors[52],
//27
neighbors[51],
//26
neighbors[50],
//25
neighbors[49],
//24
neighbors[48],
//23
neighbors[47],
//22
neighbors[46],
//21
neighbors[45],
//20
neighbors[44],
//19
neighbors[43],
//18
neighbors[42],
//17
neighbors[41],
//16
neighbors[40],
//15
neighbors[39],
//14
neighbors[38],
//13
neighbors[37],
//12
neighbors[36],
//11
neighbors[35],
//10
out7[4],
//9
out6[4],
//8
out7[4],
//7
out6[4],
//6
out5[4],
//5
out6[4],
//4
out5[4],
//3
out4[4],
//2
out5[4],
//1
out4[4],
//0
out3[4]
};

endmodule
