`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   20:58:35 08/23/2017
// Design Name:   eightxeightmem
// Module Name:   C:/Users/Joi/Desktop/Work/conways/multiplexorROM/displaymultiplex/eightxeightmem_tb.v
// Project Name:  displaymultiplex
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: eightxeightmem
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module eightxeightmem_tb;

	// Inputs
	reg [7:0] row0;
	reg [7:0] row1;
	reg [7:0] row2;
	reg [7:0] row3;
	reg [7:0] row4;
	reg [7:0] row5;
	reg [7:0] row6;
	reg [7:0] row7;
	reg Load;
	reg [2:0] addr;

	// Outputs
	wire [7:0] out;

	// Instantiate the Unit Under Test (UUT)
	eightxeightmem uut (
		.row0(row0), 
		.row1(row1), 
		.row2(row2), 
		.row3(row3), 
		.row4(row4), 
		.row5(row5), 
		.row6(row6), 
		.row7(row7), 
		.Load(Load), 
		.addr(addr), 
		.out(out)
	);

	initial begin
		// Initialize Inputs
		row0 = 0;
		row1 = 0;
		row2 = 0;
		row3 = 0;
		row4 = 0;
		row5 = 0;
		row6 = 0;
		row7 = 0;
		Load = 0;
		addr = 0;

		// Wait 100 ns for global reset to finish
		#100;
      row0 = 1;
		row1 = 2;
		row2 = 3;
		row3 = 4;
		row4 = 5;
		row5 = 6;
		row6 = 7;
		row7 = 8;
		Load = 9;
		addr = 0;
		// Add stimulus here
		Load = 1;
		#1;
		Load =0;
		addr=0;
		#1;
		addr=1;
		#1;
		addr=2;
		#1;
		addr=3;
		#1;
		addr=4;
		#1;
		addr=5;
		#1;
		addr=6;
		#1;
		addr=7;
		#1;
		
		row0 = 16;
		#1;
		Load = 1;
		#1;
		Load=0;
		#1;
		addr=0;
	end
      
endmodule

