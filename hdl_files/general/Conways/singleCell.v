`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:57:41 08/27/2017 
// Design Name: 
// Module Name:    singleCell 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//		this is the basic cell for conways game of life
//////////////////////////////////////////////////////////////////////////////////
module singleCell(
//these inputs represents the neighbors to the cells
    input [7:0] neighbor,
	 //this is the clock that tells a cell to do the next calculation
    input clk,
	 //this is the input to be loaded to a cell when load is high
	 //used for setting up the inital state
    input loadInput,
	 //the signal that tells the cell to read loadInput
    input load,
	 //the output to the display and other cells
    output reg out = 0
    );

always@( posedge clk)
begin
	if(load == 1)
	begin
		out<=loadInput;
	end
	else
	begin
	//adds all the neighbors together, if the total number of alive neighbors is less than 2 then the cell dies
	//if total is 3 then the cell becomes alive or stays alive
	//if total is greater than three then the cell dies,
	//else nothing happens
		if((neighbor[0]+neighbor[1]+neighbor[2]+neighbor[3]+neighbor[4]+neighbor[5]+neighbor[6]+neighbor[7])<2)
		begin
			out<= 0;
		end
		if((neighbor[0]+neighbor[1]+neighbor[2]+neighbor[3]+neighbor[4]+neighbor[5]+neighbor[6]+neighbor[7])==3)
		begin
			out<=1;
		end
		if((neighbor[0]+neighbor[1]+neighbor[2]+neighbor[3]+neighbor[4]+neighbor[5]+neighbor[6]+neighbor[7])>3)
		begin
			out<=0;
		end
	end
end

endmodule
