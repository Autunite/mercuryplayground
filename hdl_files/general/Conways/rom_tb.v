`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   20:26:50 08/23/2017
// Design Name:   ROM
// Module Name:   C:/Users/Joi/Desktop/Work/conways/multiplexorROM/displaymultiplex/rom_tb.v
// Project Name:  displaymultiplex
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ROM
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module rom_tb;

	// Outputs
	wire [7:0] row0;
	wire [7:0] row1;
	wire [7:0] row2;
	wire [7:0] row3;
	wire [7:0] row4;
	wire [7:0] row5;
	wire [7:0] row6;
	wire [7:0] row7;

	// Instantiate the Unit Under Test (UUT)
	ROM uut (
		.row0(row0), 
		.row1(row1), 
		.row2(row2), 
		.row3(row3), 
		.row4(row4), 
		.row5(row5), 
		.row6(row6), 
		.row7(row7)
	);

	initial begin
		// Initialize Inputs

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

