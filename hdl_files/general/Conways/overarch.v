`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:50:28 08/22/2017 
// Design Name: 
// Module Name:    overarch 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module overarch(
    input clk,
	 input reset,
	 input clkReset,
	 input [1:0] switches,
	 output slowClk,
    output [7:0] anode,
    output [7:0] cathode
    );

//input wires
wire [7:0] i0;
wire [7:0] i1;
wire [7:0] i2;
wire [7:0] i3;
wire [7:0] i4;
wire [7:0] i5;
wire [7:0] i6;
wire [7:0] i7;

//output wires
wire [7:0] r0;
wire [7:0] r1;
wire [7:0] r2;
wire [7:0] r3;
wire [7:0] r4;
wire [7:0] r5;
wire [7:0] r6;
wire [7:0] r7;

//slow clock wire
wire divClk;

//fsm to cgl wire
wire cglLoad;

//fsm to cgl clock wire
wire cglClock;

//fsm to multiplexor wire
wire multiplexorLoad;

		addrROM U0(
    .addr(switches),
	 .row0(i0),
    .row1(i1),
    .row2(i2),
    .row3(i3),
    .row4(i4),
    .row5(i5),
    .row6(i6),
    .row7(i7)
    );
	 
	 multiplexor U1(
    .row0(r0),
    .row1(r1),
    .row2(r2),
    .row3(r3),
    .row4(r4),
    .row5(r5),
    .row6(r6),
    .row7(r7),
    .load(multiplexorLoad),
	 .clk(divClk),
    .column(cathode) ,
    .row(anode) 
    );
	 
clockdiv U2(
    .clk(clk),
	 .reset(clkReset),
    .divClk(divClk)
    );
	 
	 toroidalwrapper U3(
    .clk(cglClock),
    .load(cglLoad),
    .in0(i0),
    .in1(i1),
    .in2(i2),
    .in3(i3),
    .in4(i4),
    .in5(i5),
    .in6(i6),
    .in7(i7),
    .out0(r0),
    .out1(r1),
    .out2(r2),
    .out3(r3),
    .out4(r4),
    .out5(r5),
    .out6(r6),
    .out7(r7)
    );

	overarchFSM U4(
    .loadi(reset),
    .clk(divClk),
    .loadm(multiplexorLoad),
    .loadcgl(cglLoad),
    .clocko(cglClock)
    );

assign slowClk = divClk;

endmodule
