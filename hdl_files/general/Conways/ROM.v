`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:24:13 08/22/2017 
// Design Name: 
// Module Name:    ROM 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ROM(
    output [7:0] row0,
    output [7:0] row1,
    output [7:0] row2,
    output [7:0] row3,
    output [7:0] row4,
    output [7:0] row5,
    output [7:0] row6,
    output [7:0] row7
    );

assign row0 = 8'b00000001;
assign row1 = 8'b00000001;
assign row2 = 8'b00100100;
assign row3 = 8'b00000000;
assign row4 = 8'b00000000;
assign row5 = 8'b01000010;
assign row6 = 8'b00111100;
assign row7 = 8'b00000000;

endmodule
