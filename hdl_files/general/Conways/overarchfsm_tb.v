`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   14:27:27 09/19/2017
// Design Name:   overarchFSM
// Module Name:   C:/Users/George/Documents/College/SDSU/COMPE70L/conways/multiplexorROM/displaymultiplex/overarchfsm_tb.v
// Project Name:  displaymultiplex
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: overarchFSM
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module overarchfsm_tb;

	// Inputs
	reg loadi;
	reg clk;

	// Outputs
	wire loadm;
	wire loadcgl;
	wire clocko;

	// Instantiate the Unit Under Test (UUT)
	overarchFSM uut (
		.loadi(loadi), 
		.clk(clk), 
		.loadm(loadm), 
		.loadcgl(loadcgl), 
		.clocko(clocko)
	);

	initial begin
		// Initialize Inputs
		loadi = 0;
		clk = 0;

		// Wait 100 ns for global reset to finish
		#100;
      loadi = 1;
		#20;
		loadi = 0;
		#20; 
		// Add stimulus here
		#6000000;
		loadi = 1;
		#20;
		loadi = 0;
		#20;
	end
      
	always
	#10 clk =  ~clk;  
		
endmodule

