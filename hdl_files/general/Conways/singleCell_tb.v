`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   21:19:51 08/27/2017
// Design Name:   singleCell
// Module Name:   C:/Users/George/Documents/College/SDSU/COMPE70L/conways/multiplexorROM/displaymultiplex/singleCell_tb.v
// Project Name:  displaymultiplex
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: singleCell
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module singleCell_tb;

	// Inputs
	reg [7:0] neighbor;
	reg clk;
	reg loadInput;
	reg load;

	// Outputs
	wire out;

	// Instantiate the Unit Under Test (UUT)
	singleCell uut (
		.neighbor(neighbor), 
		.clk(clk), 
		.loadInput(loadInput), 
		.load(load), 
		.out(out)
	);

	initial begin
		// Initialize Inputs
		neighbor = 0;
		clk = 0;
		loadInput = 0;
		load = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		loadInput = 1;
		load = 1;
		neighbor = 0;
		#5;
		clk = 1;
		#5;
		clk = 0;
		#5;
		
		loadInput = 0;
		load = 0;
		neighbor = 8'b11110000;
		#5;
		clk = 1;
		#5;
		clk = 0;
		#5;
		
		loadInput = 0;
		load = 0;
		neighbor = 8'b11001000;
		#5;
		clk = 1;
		#5;
		clk = 0;
		#5;
		
		loadInput = 0;
		load = 0;
		neighbor = 8'b11000000;
		#5;
		clk = 1;
		#5;
		clk = 0;
		#5;
		
		loadInput = 0;
		load = 0;
		neighbor = 8'b00001000;
		#5;
		clk = 1;
		#5;
		clk = 0;
		#5;
		
		loadInput = 0;
		load = 0;
		neighbor = 8'b10001000;
		#5;
		clk = 1;
		#5;
		clk = 0;
		#5;
	end
 //    always
//	#10 clk =  ~clk;  
endmodule

