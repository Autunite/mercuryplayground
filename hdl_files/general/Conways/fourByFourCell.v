`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:10:42 08/31/2017 
// Design Name: 
// Module Name:    fourByFourCell 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//		This combines four 2x2 cell arrays to make a 4x4 cell array
//////////////////////////////////////////////////////////////////////////////////
module fourByFourCell(
    input [43:0] neighbors,
    input clk,
    input load,
    input [3:0] in0,
    input [3:0] in1,
    input [3:0] in2,
    input [3:0] in3,
    output [3:0] out0,
    output [3:0] out1,
    output [3:0] out2,
    output [3:0] out3
    );

wire [19:0] txt00neighbor;
wire [19:0] txt10neighbor;
wire [19:0] txt01neighbor;
wire [19:0] txt11neighbor;

//upper left twobtwo
twoByTwoCell txt00(
    .neighbor(txt00neighbor),
    .clk(clk),
    .load(load),
    .in0(in0[3:2]),
    .in1(in1[3:2]),
    .out0(out0[3:2]),
    .out1(out1[3:2])
    );

//upper right twobtwo
twoByTwoCell txt10(
    .neighbor(txt10neighbor),
    .clk(clk),
    .load(load),
    .in0(in0[1:0]),
    .in1(in1[1:0]),
    .out0(out0[1:0]),
    .out1(out1[1:0])
    );

//bottom left twobtwo
twoByTwoCell txt01(
    .neighbor(txt01neighbor),
    .clk(clk),
    .load(load),
    .in0(in2[3:2]),
    .in1(in3[3:2]),
    .out0(out2[3:2]),
    .out1(out3[3:2])
    );

//bottom right twobtwo
twoByTwoCell txt11(
    .neighbor(txt11neighbor),
    .clk(clk),
    .load(load),
    .in0(in2[1:0]),
    .in1(in3[1:0]),
    .out0(out2[1:0]),
    .out1(out3[1:0])
    );
	
//assigns the two by twos to the exterior ports and to the outputs of other two by two cells	
//19 down to zero
//zero is in the upper left of the two by two, the angled corner connection coming in from the upper left
//19 is the connection directly clockwise to zero

//neighbors for the upper left twobytwo
assign txt00neighbor=
{
//19
neighbors[43],
//18
neighbors[42],
//17
neighbors[41],
//16
neighbors[40],
//15
neighbors[39],
//14
out0[1],
//13
out1[1],
//12
out0[1],
//11
out1[1],
//10
out2[1],
//9
out2[2],
//8
out2[3],
//7
out2[2],
//6
out2[3],
//5
neighbors[5],
//4
neighbors[4],
//3
neighbors[3],
//2
neighbors[2],
//1
neighbors[1],
//0
neighbors[0]
};

//neighbors for the upper right twobytwo
assign txt10neighbor=
{
//19
neighbors[37],
//18
neighbors[36],
//17
neighbors[35],
//16
neighbors[34],
//15
neighbors[33],
//14
neighbors[32],
//13
neighbors[31],
//12
neighbors[30],
//11
neighbors[29],
//10
neighbors[28],
//9
out2[0],
//8
out2[1],
//7
out2[0],
//6
out2[1],
//5
out2[2],
//4
out1[2],
//3
out0[2],
//2
out1[2],
//1
out0[2],
//0
neighbors[38]
};

//neighbors for the bottom left twobytwo
assign txt01neighbor=
{
//19
out1[3],
//18
out1[2],
//17
out1[3],
//16
out1[2],
//15
out1[1],
//14
out2[1],
//13
out3[1],
//12
out2[1],
//11
out3[1],
//10
neighbors[16],
//9
neighbors[15],
//8
neighbors[14],
//7
neighbors[13],
//6
neighbors[12],
//5
neighbors[11],
//4
neighbors[10],
//3
neighbors[9],
//2
neighbors[8],
//1
neighbors[7],
//0
neighbors[6]
};

//neighbors for the bottom right twobytwo
assign txt11neighbor=
{
//19
out1[1],
//18
out1[0],
//17
out1[1],
//16
out1[0],
//15
neighbors[27],
//14
neighbors[26],
//13
neighbors[25],
//12
neighbors[24],
//11
neighbors[23],
//10
neighbors[22],
//9
neighbors[21],
//8
neighbors[20],
//7
neighbors[19],
//6
neighbors[18],
//5
neighbors[17],
//4
out3[2],
//3
out2[2],
//2
out3[2],
//1
out2[2],
//0
out1[2]
};
	 
endmodule
