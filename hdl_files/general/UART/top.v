`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09/05/2018 01:18:39 PM
// Design Name: 
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top(
    input RX,
    output TX,
    input CLK,
    output [7:0] RX_PAR,
    input [7:0] TX_PAR
    
    );
	
	UARTWrapper U1(
    .RX(RX),
    .TX(TX),
    .Clk(CLK),
    .TX_Full(),
    .TX_Empty(),
    .RX_Full(),
    .RX_Empty(),
    .TX_IN(TX_PAR),
    .RX_Out(RX_PAR),
    .TX_LatchIn(),
    .RX_LatchOut()
    );

	
endmodule
