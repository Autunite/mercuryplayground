`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Bushnelllabs
// Engineer: George Bushnell 
// 
// Create Date: 09/05/2018 01:18:39 PM
// Design Name: 
// Module Name: TX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: This module handles the transmitting side of the
//	UART
// 
//////////////////////////////////////////////////////////////////////////////////

module TX
(
	input [7:0] TX_REG,
	input clk,
	input latchOut,
	output TX
);

endmodule