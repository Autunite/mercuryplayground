`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:25:13 08/17/2017 
// Design Name: 
// Module Name:    div8 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//This code take a 19 bit integer and shifts right 3 times to divide by 8
//////////////////////////////////////////////////////////////////////////////////
module div8(
    input [18:0] sum,
    input en,
    output reg [15:0] out =16'b1111111111111111
    );
always @(posedge en)
begin
 out <= sum>>3; 
end

endmodule
