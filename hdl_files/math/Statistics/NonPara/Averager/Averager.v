`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date:    14:27:52 08/18/2017 
// Design Name: 
// Module Name:    Averager 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Averager(
    input [15:0] Din,
    input En,
    output  [15:0] aveOut //= 16'b1111111111111111
    );
	 
//intermodule wires
//this wire connects all of the enables (clk)
//wire enable;
//this wire connects the input to the PISO
//wire [15:0] in;
//this wire connects the output of the divider to the module output
//wire [15:0] out;
//this 19 bit bundle connects the accumulator to the divider
wire [18:0] wideBundle;
//this experimental 8x16 bit wire array connects the shift register to the accumulator
wire [15:0] avBundle [7:0];
	 
accumulator U0
(
    .in0(avBundle[0]),
    .in1(avBundle[1]),
    .in2(avBundle[2]),
    .in3(avBundle[3]),
    .in4(avBundle[4]),
    .in5(avBundle[5]),
    .in6(avBundle[6]),
    .in7(avBundle[7]),
    .en(En),
    .sum(wideBundle)
);
	 
div8 U1
(
    .sum(wideBundle),
    .en(En),
    .out(aveOut)
 );
	 
SIPO1to8 U2
(
    .Din(Din),
    .En(En),
    .out0(avBundle[0]),
    .out1(avBundle[1]),
    .out2(avBundle[2]),
    .out3(avBundle[3]),
    .out4(avBundle[4]),
    .out5(avBundle[5]),
    .out6(avBundle[6]),
    .out7(avBundle[7])
);



//assign enable = En;
//assign in = Din;
//assign Out = out;

endmodule
