`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: George Bushnell
// 
// Create Date:    10:09:44 06/21/2018 
// Design Name: 
// Module Name:    MaxMin 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description:
/*
	Takes in values from the ADC and compares
	them to the current maximum and the minimum
	and if they are higher or lower to them 
	respectively then they replace the value.
	If persistence is on then this value is recorded
	and maintained since the first sample. Else it 
	remains until the 64th sample whereas the new sample
	replaces it. Devin seems to want persistence to be off
	so keep it that way.
*/

module MaxMin(
		input reset,
		input [15:0] data,
		input ADC_CLK,
		input clk,
		input Persistence,
		input Enable,
		
		output reg [15:0] Max,
		output reg [15:0] Min
		
		
	);
	
	//this reg synchronizers the reset
	//so junk data is cleared from the pipeline
	//before actually comparing things to the comparator
	reg  resetsyncer;
	reg syncer;
	
	parameter topCount = 6; 
	//parameter sets how many samples the MaxMin compares
	//over when persistence is off
	//basically specifies the number of bits the counter
	//uses
	
	//counter size is based on over how many ADC clock edges
	//you want to look over when persistence mode is
	//disabled
	reg [topCount - 1 : 0] counter;
	
	//probably only works in the FPGA
	//the cpld hates asychronous resets
	always@(posedge clk or posedge reset)
	begin
		//reset statements
		if(reset == 1)
		begin
			resetsyncer		<= 1;
			counter			<= 0;
			syncer			<= 1;
		end
		
		//statements for comparators
		else if(Enable == 1)
		begin
			
			//reset finalization
			//look for the second rising edge
			//of ADC clock for the data you seek
			if(resetsyncer != 0 && ADC_CLK == 1)
			begin
				resetsyncer	<= 0;	
			end
			
			//actual comparator code
			else if(ADC_CLK == 1)
			begin
				
				//increment counter
				counter <= counter +1;
				
				//count == 0 has special operations
				//dependend on whether persistence is on
				//or not
				if(counter == 0)
				begin
					
					//if persistence if off
					//then new values are placed
					//at the beginnign of a cycle
					if(Persistence == 0)
					begin
						Max	<= data;
						Min <= data;
					end
					
					else
					begin
						//if its the first time since reset and
						//persistence is on
						if(syncer == 1)
						begin
							syncer <= 0;
							Max	<= data;
							Min <= data;
						end
						
						else
						begin
							//comparators
					
							//max
							if(data > Max)
							begin
								Max <= data;
							end
					
							//min
							if(data < Min)
							begin
								Min <= data;
							end
						end
					end
					
				end
				
				else
				begin
					//comparators
					
					//max
					if(data > Max)
					begin
						Max <= data;
					end
					
					//min
					if(data < Min)
					begin
						Min <= data;
					end
					
					//else do nothing
					else
					begin
						
					end
					
				end
				
			end
			
			//else do nothing			
			else
			begin
				resetsyncer		<= resetsyncer;
				syncer			<= syncer;
				counter			<= counter;
				Max				<= Max;
				Min				<= Min;
			end
		
		end
		
		//else do nothing
		else
		begin
			
		end
	
	end
	
	
endmodule 