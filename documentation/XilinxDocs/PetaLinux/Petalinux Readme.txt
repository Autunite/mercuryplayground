Here lies links I have found about PetaLinux
This is so I can create a custom bsp that connects
to the wifi on start up. Has the right users and etc.

PetaLinux-Tools Wiki:
https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18841618/PetaLinux+Getting+Started

Installation files:
https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/embedded-design-tools.html

Blog about custom bsp:
https://www.zachpfeffer.com/single-post/Execute-a-Script-at-Boot-Using-PetaLinux-Tools

Adventures with PetaLinux:
https://blog.digilentinc.com/my-embedded-linux-adventure-intro-to-petalinux/

Scattered Documentation:
https://www.xilinx.com/search/support-keyword-search.html#q=PetaLinux%20Tools%20Documentation


Controlling GPIO through applets
http://lists.busybox.net/pipermail/busybox/2014-April/080824.html

Minized Linux Tutorial
https://www.hackster.io/mhanuel/minized-linux-tutorial-part-i-e2fa9d