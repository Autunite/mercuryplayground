I'm too busy to learn markdown just yet.

This isn't a real readme, but more of an informal document to show the blood
sweat and tears that I put myself through to approach and program embedded systems
so that it is comfortable for my eyes. Get a Dark Mode Xilinx.

End goal:
Xilinx Dark mode. vivado_dark.png
https://forums.xilinx.com/t5/Design-Entry/Change-colors-on-Vivado/td-p/783733
Program an fpga remotely through a beaglebone or rasberry pi


##2020/15/1
Managed to get vivado running on WSL Ubuntu.
Version 2020

One of the first issues I ran into was trying to install and run it
This website helped a lot
http://venividiwiki.ee.virginia.edu/mediawiki/index.php/ToolsXilinxLabVivadoWSL

>>to run it you have to link one of the libraries some where else
https://forum.digilentinc.com/topic/21051-vivado-missing-library-error-libtinfoso5/?sortby=date

Some official support that I didn't end up using.. yet:
https://www.xilinx.com/support/answers/66184.html?_ga=2.175790242.1813864514.1610746763-1615134554.1609461905

Recursive perl script will be included in here


